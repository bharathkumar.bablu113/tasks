#Let’s make sure our Amazon Linux 2 server is up-to-date.
sudo yum -y update
#We will install OpenJDK 11 in the server.
sudo amazon-linux-extras install java-openjdk11
#Confirm installation by checking the Java version.
java --version
#sudo tee /etc/yum.repos.d/jenkins.repo<<EOF
[jenkins]
name=Jenkins
baseurl=http://pkg.jenkins.io/redhat
gpgcheck=0
EOF
#Import GPG repository key.
sudo rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key
#The final step is the actual installation of Jenkins Server in Amazon Linux 2 system.
sudo yum install jenkins
#Start and enable the jenkins service to start at the OS boot.
sudo systemctl start jenkins
sudo systemctl enable jenkins
systemctl status jenkins
#The default login password is store in this file:
sudo cat /var/lib/jenkins/secrets/initialAdminPassword